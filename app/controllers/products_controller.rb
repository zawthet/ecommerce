class ProductsController < ApplicationController
  before_action :logged_in_user, only: [:show, :index, :edit, :update, :destroy]
  before_action :set_product, only: [:show, :edit, :update, :destroy]
 

  # GET /products
  # GET /products.json
  def index
    # @products = Product.all
     @products = Product.joins(:category).select("products.*,categories.categoryname")
      # @employees = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname").where(:employee_id => params[:id])
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
    @products = Product.find(params[:id])

  end

  def detail
    @product = Product.find(params[:id])
    @order_item = current_order.order_items.new
  end

  # def shop
  #   @order_item = current_order.order_items.new
  # end
  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to product_url, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: product_url }
      else
        format.html { render :edit }
        format.json { render json: product_url.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.jsonf
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def shop
    @products = Product.all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end
    # def detail_product_params
    #   @details = Product.find(params[:id])
    # end
    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:id, :Brand, :Model, :SKU, :Productname, :Description, :Available_size, :Available_color, :Quantity, :Picture, :category_id, :Price, :image)
    end  
  end
