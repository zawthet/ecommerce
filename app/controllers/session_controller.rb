class SessionController < ApplicationController
before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
    def new
    end
  
    def create
      user = User.find_by(email: params[:session][:email].downcase)
      if user && user.authenticate(params[:session][:password])
        if user.role == 'admin'
          redirect_to products_url
        else
          redirect_to root_path
        end
        log_in user
        # if user.activated?
        #   log_in user
        #   params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        #   redirect_back_or user
        # else
        #   message  = "Account not activated. "
        #   message += "Check your email for the activation link."
        #   flash[:warning] = message
        #   redirect_to root_url
        # end
        # log_in user
        # redirect_to products_url
      else
        flash.now[:info] = 'Invalid email/password combination'
        render 'new'
      end
    end
  
    def destroy
      log_out if logged_in?
      reset_session
      flash[:info] = "You are now logout"
        # User.find(session[:user_id]).destroy
        # session[:user_id] = nil
      redirect_to root_url
    end
end
  