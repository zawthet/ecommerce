class Category < ApplicationRecord
    has_many :products
    #belongs_to :product
    validates :categoryname,  presence: true, length: { maximum: 50 }
end
