class Product < ApplicationRecord
    belongs_to :category
    mount_uploader :image, ImageUploader
    validates :Brand,  presence: true, length: { maximum: 50 }
    validates :Model,  presence: true, length: { maximum: 50 }
    validates :SKU,  presence: true, length: { maximum: 50 }
    validates :Productname,  presence: true, length: { maximum: 50 }
    validates :Description,  presence: true, length: { maximum: 50 }
    validates :Available_size,  presence: true, length: { maximum: 50 }
    validates :Available_color,  presence: true, length: { maximum: 50 }
    validates :Quantity,  presence: true, length: { maximum: 50 }
    validates :Picture,  presence: true, length: { maximum: 100 }
    validates :Price,  presence: true, length: { maximum: 50 }
    validates :image,  presence: true

end
