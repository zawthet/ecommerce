class User < ApplicationRecord
    attr_accessor :remember_token, :activation_token
    before_save   :downcase_email
    # before_create :create_activation_digest
    validates :firstname,  presence: true, length: { maximum: 50 }
    validates :lastname, presence: true, length: { maximum: 50 }
    validates :gender, presence: true, length: { maximum: 6 }
    validates :DOB, presence: true, length: { maximum: 10 }
    
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 },
                      format: { with: VALID_EMAIL_REGEX },
                      uniqueness: { case_sensitive: false }
    validates :address, presence: true, length: { maximum: 255 }
    validates :phone, presence: true, length: { maximum: 14}
    validates :role, presence: true, length: { maximum: 5}
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
    validates :password, presence: true, length: { minimum: 6 }
  
    # Returns the hash digest of the given string.
    def User.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end
  
    # Returns a random token.
    def User.new_token
      SecureRandom.urlsafe_base64
    end
  
    # Remembers a user in the database for use in persistent sessions.
    def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))
    end
  
    # Returns true if the given token matches the digest.
    def authenticated?(attribute, token)
      digest = send("#{attribute}_digest")
      return false if digest.nil?
      BCrypt::Password.new(digest).is_password?(token)
    end
  
    def forget
      update_attribute(:remember_digest, nil)
    end
  
    # Activates an account.
    def activate
      update_columns(activated: true, activated_at: Time.zone.now)
    end
  
    # Sends activation email.
    def send_activation_email
      UserMailer.account_activation(self).deliver_now
    end
  
    private
  
      # Converts email to all lower-case.
      def downcase_email
        self.email = email.downcase
      end
  
      # Creates and assigns the activation token and digest.
    #   def create_activation_digest
    #     self.activation_token  = User.new_token
    #     self.activation_digest = User.digest(activation_token)
    #   end
  
  end