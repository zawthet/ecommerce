json.extract! product, :id, :Brand, :Model, :SKU, :Productname, :Description, :Available_size, :Available_color, :Quantity, :Picture, :created_at, :updated_at
json.url product_url(product, format: :json)
