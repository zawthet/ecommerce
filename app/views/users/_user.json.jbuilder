json.extract! user, :id, :firstname, :lastname, :gender, :DOB, :email, :address, :phone, :password, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
