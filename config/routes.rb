Rails.application.routes.draw do
  root   'products#shop'
  resources :categories
  resources :products
  # get '/new_product'
  get '/detail_product/:id', to: 'products#detail'
  get '/shop/',    to: 'order_items#shopdetail'
  resources :users
  resources :order_items
  resources :orders
  resource :cart, only: [:show]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get    '/products',to: 'products#index'
  get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  get    '/login',   to: 'session#new'
  post   '/login',   to: 'session#create'
  delete '/logout',  to: 'session#destroy'
   
  resources :users

  resources :account_activations, only: [:edit]
end

