class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :gender
      t.date :DOB
      t.string :email
      t.text :address
      t.integer :phone
      t.string :password
      t.string :role

      t.timestamps
    end
  end
end
