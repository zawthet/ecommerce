class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :Brand
      t.string :Model
      t.string :SKU
      t.string :Productname
      t.text :Description
      t.string :Available_size
      t.string :Available_color
      t.integer :Quantity
      t.string :Picture
      t.references :category

      t.timestamps
    end
  end
end
